import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import TileImage from 'ol/source/TileImage';
import TileGrid from 'ol/tilegrid/TileGrid';
import Tile from 'ol/Tile';
import { ImageTile } from 'ol';

const resolutions = [512, 256, 128, 64, 32, 16, 8, 4, 2, 1];
const extent = [0, 0, 1024000, 1024000];
const center = [512000, 512000];

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new TileImage({
        interpolate: false,
        url: 'data/world/{z}/{x}_{y}.png', // http://localhost:8080/data/world/12/2000_2000.png
        crossOrigin: null,
        wrapX: false,
        tileGrid: new TileGrid({
          extent: extent,
          resolutions: resolutions,
          tileSize: [256, 256]
        }),
      }),
      extent: extent
    }),
  ],
  view: new View({
    extent: extent,
    center: center,
    constrainResolution: true,
    zoom: 9,
    resolutions: [512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5, 0.25, 0.125]
  }),
});

// Create an overlay to display the coordinates
const coords = document.getElementById('coords')

// const overlay = new Overlay({
//   element: coords,
//   // positioning: 'bottom-left',
// });

// map.addOverlay(overlay);

// Event listener for mouse move to update the coordinates display
map.on('pointermove', (event) => {
  const coordinate = event.coordinate;
  if (coords) {
    coords.innerText = `${map.getView().getZoom()}: x: ${Math.round(coordinate[0])} y: ${Math.round(coordinate[1])} : res: ${map.getView().getResolution()}`
  }
  // coords.innerText = `<strong>Coordinates:</strong> ${hdms}`;
});


window.addEventListener('keyup', (ev) => {
  if (ev.key == "a") {
    console.log("centering");
    map.getView().animate({ zoom: 9, center: [512000, 512000] });
  }
});